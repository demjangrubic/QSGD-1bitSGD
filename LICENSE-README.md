# Licenses for Microsoft 1-bit Stochastic Gradient Descent for the Computational Network Toolkit

Microsoft 1-bit Stochastic Gradient Descent for the Computational Network Toolkit (CNTK 1bit-SGD) can be licensed according to the one of the following License Terms:

* CNTK 1bit-SGD General License
* CNTK 1bit-SGD Non-Commercial Usage License


Both Licenses are included in the root folder of the CNTK 1bit-SGD Repository.

See the descriptions of the Licenses listed above in CNTK Wiki at https://github.com/microsoft/cntk/wiki/CNTK-1bit-SGD-License

